This is an all-in-one SlimeVR board based around the ESP32-C3 and a BNO085/6
accelerometer.  It is similar to the design using the LOLIN C3 board
except IMU_INT_2 is on IO8 instead of IO7 and a non-RGB LED is connected to
GPIO 7.

This is where the similarities end, however.

===== BATTERY =====

This is designed to be able to handle fairly large lithium ion batteries
in order to provide long runtime as well as provide quick charging support.
Make sure that whatever battery is used can handle the amount of charging
current this is configured for.  The default configuration will output up to
1.5A.  In addition, the battery should always be placed against the temperature
sensor on the back of the board.

The TI BQ24259 charger is configured to charge the battery at up to 1.5A by
default, so make certain that whatever battery is used can charge at this
rate.  If a lower rate is desired, increase the value of R207.  The default
value is 316 ohms.  To change the current, use the equation:

current = 475 / R.

Unlike all of the other resistors, R210 uses an 0805 hand soldering footprint.
Someone skilled with a soldering iron should not have too much difficulty
changing this resistor value.

The maximum current can also be configured over I2C since the charger
includes i2c support.  Surprisingly, however, there does not appear to be
any way to determine the level of charge via i2c.

In the initial design, the board will only charge if turned on so that
the ESP32 can monitor charging via i2c.  If this is not a problem, a dab
of solder to JP201 on the back will allow the board to charge while powered
off.

===== Power =====

The power supply is configured to output 3.0V, not 3.3V.  This
should be fine since all components are rated to run at this voltage.

The batteryt voltage divider is also different. In order to reduce the part
count, the voltage divider is set to 143K/143K without the battery shield
resistance set to 0.

Power is controlled via the slide switch.  The switch controls the voltage
to Q201, a P-channel mosfet.  This mosfet is only controlling the
battery voltage monitor and the enable pins for the switching power supply
and the charging chip.

The output of Q201 will be pulled low when it is turned off using the
same voltage divider as is used for voltage sensing.

===== RESET =====
The reset pin on the ESP32 C3 is driven by the power good output from the
TPS63020 power supply as well as a reset button.  The PG signal will go
high when the output voltage reaches 90% of the desired 3.0V output with
a bit of additional delay.

===== Motion Sensor - BNO085 =====

The BNO085 is connected just like on the LOLIN board except in addition,
RESET and BOOT_LOAD_PIN are also connected.  These would be used if
external firmware is loaded onto the BNO085 at some future point.

===== USB =====

The USB-C connector is wired up for USB 2.0 and the CC pins are wired up
so that a source should provide at least 1.5A for charging.

===== Buttons =====
SW101 near the C3 antenna is a reset button.
SW102 at the bottom of the ESP32 C3 is a program button.  Neither should
be required.

===== Auxilliary BNO085 =====
Two connectors are present on the back.  The 5 pin connector follows the
pinout described for an external BNO085.  The second connector is compatible
with the Adafruit STEMMA QT connector.  The STEMMA connector does not
include an interrupt line, however.
The interrupt line of the auxilliary connector is connected to IO8 instead
of IO7 on the LOLIN board.

===== LEDs =====
A white LED is connected to IO7 of the ESP32.  A green and yellow LED are
to indicate charging status.

===== RS232 =====
In the event that RS232 is required, J103 can be installed which provides
a 3 pin header for a 3V RS232 connection.

===== Test Points =====
Various test points are present to test various parts of the board are
working as intended.  All test points are on the back side of the board.

TP101 is connected to the reset signal.  It should go high to bring the
ESP32 C3 out of reset.

TP102 is connected to IO0 of the ESP32 C3.  It is not used in the design.

TP103 is connected to the reset pin of the BNO085.  It should go high to
enable the BNO085.

TP104 is connected to the BNO_BOOT pin.  It should normally be high.

TP201 is the system voltage.  This is the output from the BQ24259 battery
charging chip to provide power to the system.

TP202 is the output from the TPS63020 power supply chip.  It should measure 3.0
volts.

TP203 is the switched battery output.  It shouuld be 0 when off and the
battery voltage when on.
